#!/usr/bin/env python3
#! coding: utf-8

DEBUG = False

VERSION = '0.1.0'
FILE_DEBUG = 'cfdi-debug.log'

LOG = {
    'NAME': 'CFDI',
    'FORMAT': '%(asctime)s -%(name)s-%(levelname)s - %(message)s',
    'DATE': '%d/%m/%Y %H:%M:%S',
}
if DEBUG:
    LOG['FORMAT'] = '%(asctime)s -%(name)s-%(levelname)s-%(funcName)s-%(lineno)d - %(message)s'

PREFIX = {
    '2.0': '{http://www.sat.gob.mx/cfd/2}',
    '2.2': '{http://www.sat.gob.mx/cfd/2}',
    '3.0': '{http://www.sat.gob.mx/cfd/3}',
    '3.2': '{http://www.sat.gob.mx/cfd/3}',
    '3.3': '{http://www.sat.gob.mx/cfd/3}',
    'TIMBRE': '{http://www.sat.gob.mx/TimbreFiscalDigital}',
    'NOMINA': '{http://www.sat.gob.mx/nomina}',
    'IMP_LOCAL': '{http://www.sat.gob.mx/implocal}',
    'IEDU': '{http://www.sat.gob.mx/iedu}',
    'DONATARIA': '{http://www.sat.gob.mx/donat}',
    'LEYENDAS': '{http://www.sat.gob.mx/leyendasFiscales}',
}

TAXES = {
    '000': 'exento',
    '001': 'isr',
    '002': 'iva',
    '003': 'ieps',
}

CSV_DELIMITER = '\t'
#~ BOM = '\ufeff'

